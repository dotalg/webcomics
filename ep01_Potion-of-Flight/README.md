﻿# Episode 01: Potion of Flight

![alt tag](lang/gfx_Pepper-and-Carrot_by-David-Revoy_E01.png)

2016-01-08: To compile this episode before the 2016 refactoring, you'll need older Krita sources files in 2016-01-08_ep01_Potion-of-Flight.zip (available in /src folder of the episode [http://www.peppercarrot.com/0_sources/](http://www.peppercarrot.com/0_sources/)).
